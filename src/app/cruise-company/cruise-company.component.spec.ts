import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CruiseCompanyComponent } from './cruise-company.component';

describe('CruiseCompanyComponent', () => {
  let component: CruiseCompanyComponent;
  let fixture: ComponentFixture<CruiseCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CruiseCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CruiseCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
